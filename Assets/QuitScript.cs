﻿using UnityEngine;
using System.Collections;

public class QuitScript : MonoBehaviour {

    public GameObject toffee;
    public float rotateSpeed = 180;
    private float _timer = 0;


    void Update()
    {
        _timer += Time.deltaTime;
        toffee.transform.Rotate(Vector3.back, rotateSpeed * Time.deltaTime);
        if (_timer > 3)
            Application.Quit();
    }
}
