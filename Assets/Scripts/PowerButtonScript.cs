﻿using UnityEngine;

public class PowerButtonScript : MonoBehaviour
{

    private AudioSource sound;
    private Animator animator;
    void Start()
    {
        sound = GetComponent<AudioSource>();
        animator = GetComponent<Animator>();
    }

    void Update()
    {
    }

    public void PlaySound()
    {
        if (!MainScript.Instance.playingSound)
        {
            MainScript.Instance.PlayedSound(sound, sound.clip.length);
            sound.Play();
            animator.Play("Pressed");
        }
    }
}
