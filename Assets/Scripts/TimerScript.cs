﻿using UnityEngine;
using System.Collections.Generic;
using System;
using UnityEngine.Events;

public class TimerScript : MonoBehaviour {

    public GameObject dialPrefab;
    public GameObject dotPrefab;
    public GameObject colonPrefab;
    public GameObject content;
    public bool hasDay;
    public bool hasHours;
    public bool hasMinutes;
    public bool hasSeconds;
    public List<Sprite> numbers;
    public UnityEvent alarmEvent;
    private DialScript days;
    private DialScript hourTens;
    private DialScript hourOnes;
    private DialScript minTens;
    private DialScript minOnes;
    private DialScript secTens;
    private DialScript secOnes;
    private DateTime alarmTime;
    private float minTimer=9999;
    private bool _initialized = false;
    private bool _timesUp = false;

	// Use this for initialization
	void Start () {
	    if(hasDay)
        {
            var go = Instantiate<GameObject>(dialPrefab);
            go.transform.parent = content.transform;
            go.transform.localScale = Vector3.one;
            go.transform.localPosition = new Vector3(go.transform.localPosition.x, go.transform.localPosition.y, 0);
            days = go.GetComponent<DialScript>();
            days.AddContent(numbers, 10);
            go = Instantiate(dotPrefab);
            go.transform.parent = content.transform;
            go.transform.localScale = Vector3.one;
            go.transform.localPosition = new Vector3(go.transform.localPosition.x, go.transform.localPosition.y, 0);
        }
        if (hasHours)
        {
            var go = Instantiate<GameObject>(dialPrefab);
            go.transform.parent = content.transform;
            go.transform.localScale = Vector3.one;
            go.transform.localPosition = new Vector3(go.transform.localPosition.x, go.transform.localPosition.y, 0);
            hourTens = go.GetComponent<DialScript>();
            if(!hasDay)
                hourTens.AddContent(numbers, 10);
            else
                hourTens.AddContent(numbers, 2);
            go = Instantiate<GameObject>(dialPrefab);
            go.transform.parent = content.transform;
            go.transform.localScale = Vector3.one;
            go.transform.localPosition =new Vector3( go.transform.localPosition.x, go.transform.localPosition.y, 0);
            hourOnes = go.GetComponent<DialScript>();
            hourOnes.AddContent(numbers, 10);
        }
        if (hasMinutes)
        {
            var go = Instantiate(colonPrefab);
            go.transform.parent = content.transform;
            go.transform.localPosition = new Vector3(go.transform.localPosition.x, go.transform.localPosition.y, 0);
            go.transform.localScale = Vector3.one;
            go = Instantiate<GameObject>(dialPrefab);
            go.transform.parent = content.transform;
            go.transform.localPosition = new Vector3(go.transform.localPosition.x, go.transform.localPosition.y, 0);
            go.transform.localScale = Vector3.one;
            minTens = go.GetComponent<DialScript>();
            minTens.AddContent(numbers, 6);
            go = Instantiate<GameObject>(dialPrefab);
            go.transform.parent = content.transform;
            go.transform.localPosition = new Vector3(go.transform.localPosition.x, go.transform.localPosition.y, 0);
            go.transform.localScale = Vector3.one;
            minOnes = go.GetComponent<DialScript>();
            minOnes.AddContent(numbers, 10);
        }
        if (hasSeconds)
        {
            var go = Instantiate(dotPrefab);
            go.transform.parent = content.transform;
            go.transform.localPosition = new Vector3(go.transform.localPosition.x, go.transform.localPosition.y, 0);
            go.transform.localScale = Vector3.one;
            go = Instantiate<GameObject>(dialPrefab);
            go.transform.parent = content.transform;
            go.transform.localPosition = new Vector3(go.transform.localPosition.x, go.transform.localPosition.y, 0);
            go.transform.localScale = Vector3.one;
            secTens = go.GetComponent<DialScript>();
            secTens.AddContent(numbers, 6);
            go = Instantiate<GameObject>(dialPrefab);
            go.transform.parent = content.transform;
            go.transform.localPosition = new Vector3(go.transform.localPosition.x, go.transform.localPosition.y, 0);
            go.transform.localScale = Vector3.one;
            secOnes = go.GetComponent<DialScript>();
            secOnes.AddContent(numbers, 10);
        }
    }

    void Update () {
        if(!_initialized)
        {
            SetDials();
            _initialized = true;
        }

        if (!_timesUp)
        {
            minTimer -= Time.deltaTime;
            if (minTimer <= 0)
            {
                if (hasSeconds)
                    minTimer += 1;
                else
                    minTimer += 60;
                SetDials();
            }
        }	
	}

    public void SetCountDown(DateTime alarm)
    {
        alarmTime = alarm;
        if (hasSeconds)
            minTimer = 1f - (DateTime.Now.Millisecond * 1000f) + 0.05f;
        else
            minTimer = 60 - (DateTime.Now.Millisecond * 1000f + DateTime.Now.Second) + 0.5f;
        _timesUp = false;
    }

    private void SetDials()
    {
        TimeSpan tl = alarmTime.Subtract(DateTime.Now);
        if(tl.TotalSeconds < 0.5)
        {
            alarmEvent.Invoke();
            _timesUp = true;
        }

        if (hasDay)
        {
            days.SetPosition(tl.Days);
        }
        if (hasHours)
        {
            var hoursLeft =  hasDay ? tl.Hours : (int)tl.TotalHours;
            hourTens.SetPosition(hoursLeft / 10);
            hourOnes.SetPosition(hoursLeft % 10);
        }
        if (hasMinutes)
        {
            var minsLeft = tl.Minutes;
            minTens.SetPosition(minsLeft / 10);
            minOnes.SetPosition(minsLeft % 10);
        }
        if(hasSeconds)
        {
            var secsLeft = tl.Seconds;
            secTens.SetPosition(secsLeft / 10);
            secOnes.SetPosition(secsLeft % 10, (1-tl.Milliseconds/1000f));
        }
    }
}
