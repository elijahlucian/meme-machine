﻿using UnityEngine;
using UnityEngine.Analytics;

public class SoundButtonScript : MonoBehaviour {

    public string soundId;
    private AudioSource sound;

	void Start () {
        sound = GetComponent<AudioSource>();
	}
	
	void Update () {	
	}

    public void PlaySound()
    {
        if(MainScript.Instance.playingSound)
            MainScript.Instance.StopSound();
        MainScript.Instance.PlayedSound(sound, sound.clip.length);
        Analytics.CustomEvent("played-"+soundId);
        sound.Play();
    }

    public void PlaySoundAndWait()
    {
        if (!MainScript.Instance.playingSound)
        {
            MainScript.Instance.PlayedSound(sound, sound.clip.length);
            sound.Play();
        }
    }
}
