﻿using UnityEngine;

public class LoadScript : MonoBehaviour {

    public float rotateSpeed = 180;
    private float _timer = 0; 
        

	void Update () {
        if (MainScript.Instance.GetState() == MainScript.State.Loading)
        {
            _timer += Time.deltaTime;
            gameObject.transform.Rotate(Vector3.back, rotateSpeed * Time.deltaTime);
            if (_timer > 3)
            {
                MainScript.Instance.SetState("PlayingSounds");
                _timer = 0;
            }
        }
	}
}
