﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class DialScript : MonoBehaviour {

    const int Height = 128;

    public GameObject numberPrefab;
    public GameObject content;
    private int maxNum;
    private int curNum=-1;
    private Vector3 destPos;
    private bool changePosition = false;
    private bool animate = false;
    private bool initialized = false;

	void Start () {
	}
	
	void Update () {
        if (!initialized)
            return;

        if(changePosition)
        {
            if (animate)
            {
                float delta = Math.Min(1, Time.deltaTime) * Height;
                content.transform.localPosition = new Vector3(0, Math.Max(destPos.y, content.transform.localPosition.y - delta),0);
                if (Math.Abs(destPos.y - content.transform.localPosition.y) < 1)
                {
                    if (curNum == 0)
                        content.transform.localPosition = new Vector3(0, maxNum * Height, 0);
                    animate = false;
                    changePosition = false;
                }
            }
            else
            {
                content.transform.localPosition = destPos;
                changePosition = false;
            }
        }
	}


    public void SetPosition(int newvalue, float percent=0f)
    {
        if (newvalue < 0 || newvalue >= maxNum || curNum == newvalue)
            return;

        if (curNum != -1)
            percent = 0;
        if(newvalue == maxNum-1)
            destPos = new Vector3(0, (newvalue-percent) * Height, 0);
        else
            destPos = new Vector3(0, (newvalue - percent) * Height, 0);
        changePosition = true;
        animate = (curNum != -1);
        curNum = newvalue;
    }

    public void AddContent(List<Sprite>sprites, int max)
    {
        maxNum = max;

        GameObject go;
        for(int i=0; i<max; i++)
        {
            go = (GameObject)Instantiate<GameObject>(numberPrefab);
            go.GetComponent<Image>().sprite = sprites[i];
            go.transform.parent = content.transform;
            go.transform.localScale = Vector3.one;
            go.transform.localPosition = Vector3.zero;
        }
        go = (GameObject)Instantiate<GameObject>(numberPrefab);
        go.GetComponent<Image>().sprite = sprites[0];
        go.transform.parent = content.transform;
        go.transform.localScale = Vector3.one;
        go.transform.localPosition = Vector3.zero;
        initialized = true;
    }
}
