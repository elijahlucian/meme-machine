﻿using UnityEngine;
using System.Collections;
using System;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;
using GameSparks.Api;
using GameSparks.Core;
using GameSparks.Api.Messages;

public class MainScript : MonoBehaviour {

    public static MainScript Instance;
    public enum State { Splash, Loading, PlayingSounds, Show, About, Quitting };

    public GameObject AudioPage;
    public GameObject LoadingPage;
    public GameObject AboutPage;
    public GameObject ShowPage;
    public GameObject QuitPage;

    public bool playingSound;
    private float playTime = 0;
    private AudioSource audioSource;

	void Start () {
        if (Instance == null)
            Instance = this;
        SetState("Loading");

        GSMessageHandler._AllMessages = HandleGameSparksMessageReceived;
        new DeviceAuthenticationRequest().Send(handleAuthentication);
    }

    private void handleAuthentication(AuthenticationResponse obj)
    {
        if(obj.HasErrors)
        {

        }
        else
        {
            Debug.Log("Successfully authenticated device");
        }
    }

    void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
            Back();

        if (_state == State.PlayingSounds)
        {
            if (playTime > 0)
            {
                playTime -= Time.deltaTime;
                if (playTime < 0)
                    playingSound = false;
            }
        }
    }

    void HandleGameSparksMessageReceived(GSMessage message)
    {
        Debug.Log("MSG:" + message.JSONString);
    }

    private State _state = State.Splash;
    public void SetState(string state)
    {
        State newstate = (State)Enum.Parse(typeof(State), state);
        if (newstate != _state)
        {
            LoadingPage.SetActive(newstate == State.Loading);
            AudioPage.SetActive(newstate == State.PlayingSounds);
            ShowPage.SetActive(newstate == State.Show);
            AboutPage.SetActive(newstate == State.About);
            QuitPage.SetActive(newstate == State.Quitting);
            _state = newstate;
        }
    }

    public void ToggleHeader()
    {
        switch(_state)
        {
            case State.Show:
                SetState("PlayingSounds");
                break;
            case State.PlayingSounds:
                SetState("Show");
                break;
        }
    }

    public State GetState()
    {
        return _state;
    }

    public void PlayedSound(AudioSource audio, float time)
    {
        audioSource = audio;
        playingSound = true;
        playTime = time;
    }

    public void StopSound()
    {
        audioSource.Stop();
    }

    public void LauchUrl(string url)
    {
        Application.OpenURL(url);
    }

    public void Back()
    {
        switch(_state)
        {
            case State.Show:
                SetState("PlayingSounds");
                break;
            case State.About:
                SetState("Show");
                break;
            case State.PlayingSounds:
                SetState("Quitting");
                break;
        }
    }
}
