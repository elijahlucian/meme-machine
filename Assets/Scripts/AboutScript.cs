﻿using UnityEngine;
using System;

public class AboutScript : MonoBehaviour {

    public TimerScript showTimer;
    public GameObject backButton;
    public GameObject OnAirButton;
    private DateTime _resumeTime;
    private bool _initialized = false;
    private int _lastMinute;

    void OnEnable()
    {
        _initialized = false;
#if UNITY_IOS || UNITY_EDITOR
        backButton.SetActive(true);
#else
        backButton.SetActive(false);
#endif
    }

    void Update () {
	    if(!_initialized)
        {
            _initialized = true;
            DateTime dt;
            try
            {
                TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById("Mountain Standard Time");
                dt = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, tzi.IsDaylightSavingTime(DateTime.Now) ?
                        tzi.DaylightName : tzi.StandardName);
            }
            catch(Exception e)
            {
                dt = DateTime.UtcNow.AddHours(-7);
            }

            if (dt.DayOfWeek != DayOfWeek.Monday || dt.Hour > 19)
            {
                int offset = 0;
                switch (dt.DayOfWeek)
                {
                    case DayOfWeek.Saturday:
                        offset = 2;
                        break;
                    case DayOfWeek.Sunday:
                        offset = 1;
                        break;
                    case DayOfWeek.Monday:
                        offset = 7;
                        break;
                    case DayOfWeek.Tuesday:
                        offset = 6;
                        break;
                    case DayOfWeek.Wednesday:
                        offset = 5;
                        break;
                    case DayOfWeek.Thursday:
                        offset = 4;
                        break;
                    case DayOfWeek.Friday:
                        offset = 3;
                        break;
                }
                dt = dt.AddDays(offset);
            }
            showTimer.gameObject.SetActive(true);
            showTimer.SetCountDown(new System.DateTime(dt.Year, dt.Month, dt.Day, 19, 0, 0));
            _resumeTime = new DateTime(dt.Year, dt.Month, dt.Day, 21, 0, 0);
            _lastMinute = DateTime.Now.Minute;
        }

        if(OnAirButton.activeInHierarchy && (_lastMinute != DateTime.Now.Minute))
        {
            TimeSpan tl = _resumeTime.Subtract(DateTime.Now);
            if (tl.TotalSeconds < 0.5)
            {
                _initialized = false;
                showTimer.gameObject.SetActive(true);
                OnAirButton.SetActive(false);
            }
        }
    }
}
