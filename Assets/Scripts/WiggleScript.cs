﻿using UnityEngine;
using System.Collections;

public class WiggleScript : MonoBehaviour {

    private float _wiggleTime;
    private float _restTime;
    private bool _wiggling;

	void Start () {
	}

    void OnEnable()
    {
        _wiggling = false;
        _restTime = 1f;
    }
    void Update ()
    {
	    if(_wiggling)
        {
            _wiggleTime -= Time.deltaTime;
            if(_wiggleTime <=0)
            {
                _wiggling = false;
                _restTime = 2f;
            }
        }   
        else
        {
            _restTime -= Time.deltaTime;
            if(_restTime <=0)
            {
                _wiggleTime = 1.5f;
                _wiggling = true;
                GetComponent<Animation>().Play();
            }
        } 
	}
}
